﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using jvd.samples.mvc4.Models.Data;

namespace jvd.samples.mvc4.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
        //
        // GET: /Home/
        public ActionResult GetSearchResults(Models.Criteria.ChronosEntryCriteria criteria)
        {
            var chronos = new ChronosEntities();

            var model = criteria.GetResults(chronos.Chronos_Entry);

            return PartialView("_resultsTable", model);
        }
    }
}
