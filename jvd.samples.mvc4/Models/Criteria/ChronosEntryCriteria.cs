﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace jvd.samples.mvc4.Models.Criteria
{
    public class ChronosEntryCriteria
    {
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string UserName { get; set; }
        public string SortColumn { get; set; }
        public bool SortDescending { get; set; }
        int PageSize = 50;
        int PageNumber = 0;

        public IQueryable<Models.Data.Chronos_Entry> GetResults(IQueryable<Models.Data.Chronos_Entry> entries)
        {
            entries = entries
                        .Include(entry => entry.Chronos_Activity)
                        .Include(entry => entry.Core_Project)
                        .Include(entry => entry.Core_Organization);

            entries = StartDate.HasValue ? entries.Where(e => e.EntryDate >= this.StartDate.Value) : entries;
            entries = EndDate.HasValue ? entries.Where(e => e.EntryDate <= this.EndDate.Value) : entries;
            entries = string.IsNullOrEmpty(this.UserName) ? entries : entries.Where(e => e.Core_User.UserName == this.UserName);

                switch (this.SortColumn)
                {
                    case "EntryDate":
                            default:
                        entries = this.SortDescending ? entries.OrderByDescending(e => e.EntryDate) : entries.OrderBy( e => e.EntryDate);
                        break;
                    case "Project":
                        entries = this.SortDescending ? entries.OrderByDescending(e => e.EntryDate) : entries.OrderBy(e => e.Core_Project.ProjectName);
                        break;
                }

                entries = entries.Skip(PageSize * PageNumber).Take(PageSize);

                return entries;
        }
    }
}